package com.arc.marketPlace.pages;

import java.awt.AWTException;
import java.awt.HeadlessException;
import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.LoadableComponent;
import org.testng.Assert;

import com.arc.marketPlace.utils.Generate_Random_Number;
import com.arc.marketPlace.utils.SkySiteUtils;
import com.arcautoframe.utils.Log;
import com.steadystate.css.parser.ParseException;



public class RegistrationPage extends LoadableComponent<RegistrationPage> {
	
	WebDriver driver;
	private boolean isPageLoaded;
	
	/**
	 * Identifying web elements using FindBy annotation.
	 */
	
		
	//==========Free Trail===============
	@FindBy(css=".head-signup.menu-item>a")
	WebElement FreeTrailLink;
	
	@FindBy(css="#fullname")
	WebElement FullName;
	
	@FindBy(css="#state-show")
	WebElement State;
	
	@FindBy(xpath=".//*[@id='free-trial']/div[1]/div/div/div/div[2]/div/div/div/ul/li[1]")
	WebElement State_index1;
	
	@FindBy(css="#phone")
	WebElement PhoneNumber;
	
	@FindBy(css="#email")
	WebElement Email;
	
	@FindBy(css="#password")
	WebElement Password;
	
	@FindBy(css="#free-trial-btn")
	WebElement CompleteMyRegistration;
	
	@FindBy(xpath="//a[contains(text(),'Log out')]")
	WebElement LogOut;
	
	
	@FindBy(xpath="//span[@class='img-circle profile-no-image md']")
	WebElement profile;
	
	
	
	@FindBy(css="#button-1")
	WebElement YesButton;
	
	
	
	@Override
	protected void load() {
		isPageLoaded = true;
		SkySiteUtils.waitForElement(driver, FreeTrailLink, 20);
		
	}

	@Override
	protected void isLoaded() throws Error {
		if (!isPageLoaded) {
			Assert.fail();
		}
	}
	
	/**
	 * Declaring constructor for initializing web elements using PageFactory class.
	 * @param driver
	 * @return 
	 */
	public RegistrationPage(WebDriver driver) 
	{
		   this.driver = driver;
		   PageFactory.initElements(this.driver, this);
	}
	
	
	
	/** 
	 * Method written for checking whether User Name text box is present?
	 * @return
	 */
	public boolean MainProjects()
	{
		SkySiteUtils.waitForElement(driver, FreeTrailLink, 20);
		Log.message("Waiting for FreeTrail Link to be appeared");
		if(FreeTrailLink.isDisplayed())
			return true;
			else
			return false;
	}
	
		
	
	//======================================================================================
	
	public boolean FreeTrailLink() throws HeadlessException, AWTException, IOException
	
	{
		
		//CommonMethod.waitForJSandJQueryToLoad();
		SkySiteUtils.waitTill(2000);
		SkySiteUtils.waitForElement(driver, FreeTrailLink,60);
		Log.message("Waiting for FreeTrailLink  to be appeared");
		FreeTrailLink.click();
		Log.message("Free Trail Link to Be clicked Sucessfully");
		
		if(FreeTrailLink.isDisplayed())
			return true;
			else
			return false;
		
	
	}
	/** 
	 * Method written By Ranjan
	 * @return 
	
	 * @return
	 * @throws java.text.ParseException 
	 * @throws ParseException 
	 * @throws AWTException 
	 * @throws InterruptedException 
	 */
	public boolean RegistrationPage(String fullName,String state, String phoneNumber ,String password) throws  java.text.ParseException, AWTException, InterruptedException, ParseException
	
	{
		
		SkySiteUtils.waitForElement(driver, FullName, 60);
		Log.message("Waiting for FullName Inbox to be appeared");
		//======Entered Value In FullName TextBox=============
		FullName.clear();
		FullName.sendKeys(fullName);
		Log.message("FullName: " + fullName + " " + "has been entered in Full Name text box." );
		//String pWord = PropertyReader.getProperty("Password");
		 //======Entered Value In State TextBox==================
		
		State.clear();
		State.sendKeys(state);
		SkySiteUtils.waitTill(6000);
		SkySiteUtils.waitForElement(driver, State_index1,30);
		State_index1.click();
		
		Log.message("State: " + state + " " + "has been entered in state text box." );
		
		//======Entered Value In Phone Number TextBox============
		SkySiteUtils.waitForElement(driver, PhoneNumber, 80);
		PhoneNumber.clear();
		PhoneNumber.sendKeys(phoneNumber);
		Log.message("PhoneNumber: " + phoneNumber + " " + "has been entered in phoneNumber text box." );
		//======Entered Value In Email TextBox====================
		String email= Generate_Random_Number.RandomEmail();
		Email.clear();
		Email.sendKeys(email);
		Log.message("Email: " + email + " " + "has been entered in Email text box." );
		
		//======Entered Value In Password TextBox=================
		Password.clear();
		Password.sendKeys(password);
		Log.message("Password: " + password + " " + "has been entered in Password text box." );
		
		//============== complete My registration Button Clicked=========================
		SkySiteUtils.waitForElement(driver, CompleteMyRegistration, 180);
		
		CompleteMyRegistration.click();
		Log.message("Complete My registration button Clicked Sucessfully");
		SkySiteUtils.waitTill(5000);		
		/*if(CompleteMyRegistration.isDisplayed()){
		return true;}
		else{
		return false;}*/
		return true;
		
	}
	

	/** 
	 * Method written By Ranjan
	
	 * @return
	 */
	
public ProjectDashboardPage LogOut()
	
	{
	     SkySiteUtils.waitTill(5000);
	    SkySiteUtils.waitForElement(driver, profile, 80);
	    profile.click();
	    Log.message("Profile Clicked Sucessfully");
	    SkySiteUtils.waitTill(5000);
		SkySiteUtils.waitForElement(driver, LogOut, 100);			
		LogOut.click();	
		Log.message("Logout button Clicked Sucessfully");
		SkySiteUtils.waitForElement(driver, YesButton, 30);
		Log.message("Waiting for yes Button Present");
		YesButton.click();
		Log.message("Yes Button Clicked Sucessfully ");
		return new ProjectDashboardPage(driver).get();		
		
		
	}


	
	
	
	
}
	
	
	